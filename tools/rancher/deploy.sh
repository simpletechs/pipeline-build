#!/bin/bash

# run as $0 "stack/service-name" "image_with_tag"
# prepare for rancher-gitlab-deploy -> https://github.com/cdrx/rancher-gitlab-deploy
# RANCHER_URL is also used
export RANCHER_ACCESS_KEY=$RANCHER_KEY
export RANCHER_SECRET_KEY=$RANCHER_SECRET

set -e

# todo: catch errors, run revoke regardless but exit after that
/tools/aws/authorize-current-ip.sh || true

# run deploy command and store the exit code
SPLITTED=(${1//\// })
STACK=${SPLITTED[0]} 
SERVICE=${SPLITTED[1]} 
TIMEOUT=${RANCHER_TIMEOUT:-180}
BATCH_INTERVAL=${RANCHER_BATCH_INTERVAL:-10}

START_BEFORE_STOP=${RANCHER_START_BEFORE_STOP:-1}
START_BEFORE_ARG="--start-before-stopping"
if [ "$START_BEFORE_STOP" -eq "0" ]; then
    START_BEFORE_ARG="--no-start-before-stopping"
fi


ENV_ARG=""
if [ -n $RANCHER_ENVIRONMENT ]; then 
    ENV_ARG="--environment $RANCHER_ENVIRONMENT"; 
fi
echo "Deploying service \"$SERVICE\" in stack \"$STACK\" (in env $RANCHER_ENVIRONMENT): $2"
echo "Timeout is set to $TIMEOUT seconds..."


echo ""

# so weird because we want to use set -e to catch script errors
DEPLOY_EXIT_CODE=0
set -x
(rancher-gitlab-deploy --stack "$STACK" --service "$SERVICE" $ENV_ARG --new-image "$2" --wait-for-upgrade-to-finish --upgrade-timeout "$TIMEOUT" --finish-upgrade $START_BEFORE_ARG --batch-interval "$BATCH_INTERVAL" --rollback-on-error ${@:3}) || DEPLOY_EXIT_CODE=$?
set +x

echo "ran deploy script with exit code $DEPLOY_EXIT_CODE"

/tools/aws/authorize-current-ip.sh revoke || true

# let the pipeline know if something is wrong
exit $DEPLOY_EXIT_CODE