#!/bin/bash
#set -x

# Settings
CACHE_GIT_REPO=$PIPELINES_CACHE_GIT_REPO
CACHE_DIRECTORY=/tmp/docker-cache
SOURCE_DIRECTORY=`pwd`

# Args
# -n name of the cache

# either
#  -f name of the cache-file (i.e. package.json) that must change
#  -c command to run on change
# or
#  -t type (i.e. npm, yarn)

while getopts ":n:f:c:t:" opt; do
    case $opt in
        n)
            CACHE_NAME="$OPTARG"
            ;;
        f)
            CACHE_FILE_NAME="$OPTARG"
            ;;
        c)
            CACHE_MISS_COMMAND="$OPTARG"
            ;;
        t)
            case "$OPTARG" in
                npm)
                    CACHE_MISS_COMMAND="npm install"
                    CACHE_FILE_NAME="package.json"
                    ;;
                yarn)
                    CACHE_MISS_COMMAND="yarn"
                    CACHE_FILE_NAME="package.json"
                    ;;
                bower)
                    CACHE_MISS_COMMAND="bower --allow-root install"
                    CACHE_FILE_NAME="bower.json"
                    ;;
                *)
                    echo "Invalid type $OPTARG" >&2
                    exit 1
                    ;;
            esac
            ;;
        *)
            echo "Invalid type $OPTARG" >&2
            exit 1
            ;;
    esac
done

if [ -z "$CACHE_NAME" ]; then
    echo "Cache-Name (-n) is required."
    exit 1
fi

if [ -z "$CACHE_FILE_NAME" ]; then
    echo "Cache-File (-f) is required."
    exit 1
fi

if [ -z "$CACHE_MISS_COMMAND" ]; then
    echo "Cache Miss Command (-c) is required."
    exit 1
fi

printf "Running with options:\nCache-Name: %s\nCache-File: %s\nCache Miss Command: %s\n" "$CACHE_NAME" "$CACHE_FILE_NAME" "$CACHE_MISS_COMMAND"

CACHE_CHECKSUM_FILE_NAME=".$CACHE_FILE_NAME.sha512"

# Clone docker cache dir
rm -rf $CACHE_DIRECTORY
mkdir -p $CACHE_DIRECTORY
cd $CACHE_DIRECTORY

branch_exists=`git ls-remote --exit-code $CACHE_GIT_REPO $CACHE_NAME`
if [ ! "$branch_exists" == "" ]; then
    echo "branch $CACHE_NAME already exists"
    git clone --depth 1 -b $CACHE_NAME $CACHE_GIT_REPO .
    git reset --hard
    git clean -d -f
else
    echo "creating new branch $CACHE_NAME"
    git clone --depth 1 -b master $CACHE_GIT_REPO .
    git checkout --orphan $CACHE_NAME
    git rm --cached $(git ls-files)
    git clean -d -f
fi
git status

# check if cache-file changed
cd $SOURCE_DIRECTORY
sha512sum -s -c "$CACHE_DIRECTORY/$CACHE_CHECKSUM_FILE_NAME"
checksum_matches=$?

if [ ! "$checksum_matches" == "0" ]; then
    echo "changes detected in $CACHE_FILE_NAME, rebuilding the cache"
    # add cache-file to the mix
    sha512sum $SOURCE_DIRECTORY/$CACHE_FILE_NAME > "$CACHE_DIRECTORY/$CACHE_CHECKSUM_FILE_NAME"
    cp $SOURCE_DIRECTORY/$CACHE_FILE_NAME $CACHE_DIRECTORY/

    # run the install command
    cd $CACHE_DIRECTORY
    eval $CACHE_MISS_COMMAND

    # commit changes to git
    git add --all .
    git commit -am "cache updated due to changes in $CACHE_FILE_NAME"
    git push --all origin
else
    echo "no changes."
fi

cd $CACHE_DIRECTORY
# cleanup before copying to source
rm -rf "$CACHE_CHECKSUM_FILE_NAME" .git $CACHE_FILE_NAME

# copy cache to source directory
# if cp fails (this may happen on empty cache dir, discard the error)
cp -aR $CACHE_DIRECTORY/* $SOURCE_DIRECTORY || true
