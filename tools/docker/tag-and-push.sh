#!/bin/bash
# run as: $0 "repo/image:tag" "tag" "[if set]"

sanitize() {
   local s="${1?need a string}" # receive input in first argument
   s="${s//[^[:alnum:].]/-}"    # replace all non-alnum characters to -
   s="${s//+(-)/-}"             # convert multiple - to single -
   s="${s/#-}"                  # remove - from start
   s="${s/%-}"                  # remove - from end
   echo "${s}"
}

IMAGE=$1
# sanitize tag
NEW_TAG=`sanitize "$2"`

if [ -z "$3" ]; then
    echo "condition not fulfilled to tag $IMAGE as $NEW_TAG"
    exit 0
fi

echo "Tagging and pushing image as $IMAGE:$NEW_TAG"
docker tag "$IMAGE" "$IMAGE:$NEW_TAG"
docker push "$IMAGE:$NEW_TAG"

if [ -z ${BITBUCKET_BUILD_NUMBER+x} ]; then
    echo "No BITBUCKET_BUILD_NUMBER set, skipping additional tagging"
else
    echo "Also tagging and pushing as build-${BITBUCKET_BUILD_NUMBER}"
    docker tag "$IMAGE" "$IMAGE:build-${BITBUCKET_BUILD_NUMBER}"
    docker push "$IMAGE:build-${BITBUCKET_BUILD_NUMBER}"
fi