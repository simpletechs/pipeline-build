#!/bin/bash
# use as: retag.sh repository old-tag new-tag

REPOSITORY="$1"
TAG_OLD="$2"
TAG_NEW="$3"

function retag {
    _REPOSITORY="$1"
    _TAG_OLD="$2"
    _TAG_NEW="$3"

    echo "Retagging ${_REPOSITORY}:${_TAG_OLD} as ${_REPOSITORY}:${_TAG_NEW}"
    MANIFEST=$(aws ecr batch-get-image --repository-name "${_REPOSITORY}" --image-ids imageTag="${_TAG_OLD}" --output json | jq --raw-output --join-output '.images[0].imageManifest')
    
    # retag using the AWS CLI. If this succeeds, exit without an error code
    aws ecr put-image --repository-name "${_REPOSITORY}" --image-tag "${_TAG_NEW}" --image-manifest "$MANIFEST" && exit 0
}

if [ -z ${BITBUCKET_BUILD_NUMBER+x} ]; then
    echo "No BITBUCKET_BUILD_NUMBER set, falling back to deprecated behavior."
else
    echo "A BITBUCKET_BUILD_NUMBER is set, trying to retag using tag build-${BITBUCKET_BUILD_NUMBER}"
    retag "$REPOSITORY" "build-${BITBUCKET_BUILD_NUMBER}" "$TAG_NEW" || echo "No image with tag build-${BITBUCKET_BUILD_NUMBER} found, falling back to retagging $TAG_OLD as $TAG_NEW" 
fi

retag "$REPOSITORY" "$TAG_OLD" "$TAG_NEW"