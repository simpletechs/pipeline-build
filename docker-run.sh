#!/bin/sh

mkdir -p /aws

# Install aws cmdline tool
apk -Uuv add vim groff less aws-cli jq
rm /var/cache/apk/*