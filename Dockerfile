FROM simpletechs/alpine-node-pipelines:latest

LABEL maintainer="Fabian Off <fabian+docker-pipelines@simpletechs.net>"

# Install aws cmdline tool
RUN \
    mkdir -p /aws && \
    apk -Uuv add vim groff less python3 py-pip py3-pip aws-cli && \
    pip3 install rancher-gitlab-deploy && \
    apk --purge -v del py-pip py3-pip && \
    rm /var/cache/apk/*

ADD ./tools /tools

# Set default container command
ENTRYPOINT /bin/bash